
## V0.2.0 (2022-11-29)

* Implemented `padding` option
* Added EuroRegionalMap bridge (cubewerx) to demo
* Part of the 19th OGC codespring held on 2022-11-29

## v0.1.0 (2022-03-10)

* Initial release
* Part of the OGC/OSGeo workshop held on 2022-03-08
* Basic functionality: refresh image and bounds on `moveend`
* Basic attribution handling, no sanitization of provided HTML

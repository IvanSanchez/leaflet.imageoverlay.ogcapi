
# Leaflet.ImageOverlay.OGCAPI

Leaflet plugin to displays untiled map images from a OGC API Maps service.


This LeafletJS plugin adds a new class, `L.ImageOverlay.OGCAPI`, subclass of
[`L.ImageOverlay`](http://leafletjs.com/reference.html#imageoverlay). Every time
the map viewport (center/zoom) changes, the image will be reloaded and its
bounds will be updated to fit the map's viewport.

The image reloading happens at every `moveend` event, and the `padding` option allows to cache a small area around the viewport. This means that a timeout or debounce is not deemed necessary.

## Demo

https://ivansanchez.gitlab.io/leaflet.imageoverlay.ogcapi/demo.html

### Loading the plugin

Load the plugin by adding the appropriate `<script>` HTML tag, for example:

```html
<script src='https://unpkg.com/leaflet-imageoverlay-ogcapi@0.1.0/Leaflet.ImageOverlay.OGCAPI.js'></script>
```

The plugin is available as an NPM package with the name
`leaflet-imageoverlay-ogcapi`; you might prefer to run
`npm install leaflet-imageoverlay-ogcapi` if you're using a build system, or
you might prefer to manually copy the `Leaflet.ImageOverlay.OGCAPI.js` from
this repository.

### Usage

To instantiate a `L.ImageOverlay.OGCAPI`, specify the base URL of the OGC API,
and the **ID** (not the name, not the title) of the "collection" in the OGC API.

For example:

```js
var overlay = L.imageOverlay.ogcapi("https://maps.ecere.com/ogcapi/", {
	collection: "NaturalEarth:raster:NE1_HR_LC_SR_W_DR",
}).addTo(map);
```

Optionally, you can specify:
* The MIME type of the image format to request, e.g. `image/png` or `image/jpeg`.
  If ommitted, the first format listed in the collection metadata will be used.
* A `padding` amount, with the same semantics as the `padding` option of
  `L.Renderer`. The default of `0.1` means that the requested image will be 10%
  more than needed in each of the north, south, east and west directions.
* A boolean `transparent` flag, to request a transparent (or opaque) background
  on the images.


For example:

```js
var overlay = L.imageOverlay.ogcapi("https://maps.ecere.com/ogcapi/", {
	collection: "NaturalEarth:raster:NE1_HR_LC_SR_W_DR",
	imageFormat: "image/jpeg",
	padding: 0.1,
	transparent: true,
}).addTo(map);
```

### Caveats

This implementation is based on the not-yet-approved OGC API Maps specification,
as of March 2022. The known documentation used is:
- https://docs.ogc.org/DRAFTS/20-058.html
- https://opengeospatial.github.io/architecture-dwg/api-maps/index.html

This implementation only works when the Leaflet map is using the default
`L.CRS.EPSG3857` crs (not `L.CRS.EPSG4326`, and not `proj4leaflet`).

### Legalese

The code for this plugin is under a Beerware license:

----------------------------------------------------------------------------

"THE BEER-WARE LICENSE":
<ivan@sanchezortega.es> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return.

----------------------------------------------------------------------------



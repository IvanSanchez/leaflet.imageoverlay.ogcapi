/***************************************************************************

"THE BEER-WARE LICENSE":
<ivan@sanchezortega.es> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return.

***************************************************************************/

/**
 * @class L.ImageOverlay.OGCAPI
 *
 * A `ImageOverlay` that displays conformal rasters from a OGC API Maps service,
 * refreshing the bounds as to fit the map viewport.
 *
 * It's designed against the OGC API Maps described at:
 * - https://docs.ogc.org/DRAFTS/20-058.html
 * - https://opengeospatial.github.io/architecture-dwg/api-maps/index.html
 *
 * The constructor needs the base URL of the OGC API endpoint, and the ID of a
 * "collection".
 *
 * This is an **untiled** client implementation: every viewport change
 * translates into a new image request to the API.
 *
 * This implementation assumes that the Leaflet map is using its default
 * CRS (EPSG:3857). There is no support for other CRSs (neither L.CRS.EPSG4326
 * nor Proj4Leaflet).
 *
 */

L.ImageOverlay.OGCAPI = L.ImageOverlay.extend({
	// @option collection: String
	// The ID (*not* the name/title) of the collection to use.
	// @option transparent: Boolean
	// Whether or not to request transparent images.
	// @option padding: Number = 0.1

	initialize: function initialize(url, opts) {
		const collection = opts.collection;

		this._padding = opts.padding ?? 0.1;

		// Strip the trailing slash, if there's one.
		this._baseURL = url.replace(/\/?$/, "");

		// First, fetch the metadata for the collection
		// The collection metadata is common for all OGC APIs related to that
		// collection - e.g. raster maps, raster tiles, raw vector features,
		// are all listed in the collection's metadata.
		const collectionsURL = new URL(this._baseURL + "/collections", document.URL);
		this._collection = fetch(collectionsURL, {
			headers: {
				Accept: "application/json",
			},
		})
			.then((response) => response.json())
			.then((json) => {
				// console.log(json.collections);
				const matches = json.collections.filter((c) => c.id === collection);
				if (matches.length === 0) {
					throw new Error(
						`Collection '${collection}' is not available from OGC API endpoint ${this._baseURL}`
					);
				} else if (matches.length > 1) {
					throw new Error(
						`Collection '${collection}' has a duplicate definition from OGC API endpoint ${this._baseURL}`
					);
				}

				const collectionData = matches[0];

				// Attribution is a bit tricky: Leaflet doesn't have capabilities
				// to update the attribution of an existing L.Layer; instead,
				// it'll be removed and re-added.
				// Note that, even though this code is within `initialize()`, it
				// it runs async, and therefore the instance might have already
				// been added to the map.
				// TODO: There are some concerns about pulling API-provided
				// HTML as attribution without sanitizing the HTML first.
				if ("attribution" in collectionData) {
					const map = this._map;
					if (map) {
						this.remove();
					}
					this.options.attribution = collectionData.attribution;
					if (map) {
						this.addTo(map);
					}
				}
				return collectionData;
			});

		// Then, choose an appropriate image endpoint for the collection,
		// based on the image format.
		this._imageURL = this._collection
			.then((col) => {
				// Filter links: only interested in the ones conforming to the
				// OGC API Maps spec.
				const links = col.links.filter(
					(l) => l.rel === "http://www.opengis.net/def/rel/ogc/1.0/map"
				);
				if (!opts.imageFormat) {
					// On no specified image format (png/jpeg), use the first one from
					// the collection metadata
					return links[0];
				} else {
					const formatLinks = links.filter((l) => l.type === opts.imageFormat);
					if (formatLinks.length === 0) {
						throw new Error(
							`OGC API: Maps for collection ${collection} are not available in image format ${opts.imageFormat}`
						);
					} else if (formatLinks.length > 1) {
						throw new Error(
							`OGC API: Maps for collection ${collection} have multiple endpoints for image format ${opts.imageFormat}`
						);
					}
					return formatLinks[0];
				}
			})
			.then((link) => {
				return new URL(link.href, this._baseURL);
			});

		this._imageURL.catch((ex) => {
			throw new Error(`Could not get details from OGC API because: ${ex}`);
		});

		L.Util.setOptions(this, opts);
	},

	onAdd: function (map) {
		if (!this._bounds) {
			this._bounds = map.getBounds();
		}
		map.on("moveend", this._reloadImage, this);
		map.on("zoomend", this._forceReloadImage, this);
		this._reloadImage();
		this._url = "";
		this._lastBounds = L.latLngBounds([
			[0, 0],
			[0, 0],
		]);
		return L.ImageOverlay.prototype.onAdd.call(this, map);
	},

	onRemove: function () {
		this._map.off("moveend", this._reloadImage, this);
		this._map.off("zoomend", this._forceReloadImage, this);
		return L.ImageOverlay.prototype.onRemove.call(this);
	},

	_forceReloadImage() {
		this._lastBounds = L.latLngBounds([
			[0, 0],
			[0, 0],
		]);
		return this._reloadImage();
	},

	_reloadImage() {
		this._imageURL.then((url) => {
			// Sanity check (it'd be possible to hit a race condition)
			if (!this._map) {
				return;
			}

			if (this._lastBounds.contains(this._map.getBounds())) {
				return;
			}

			const bounds = this._map.getBounds().pad(this._padding);
			const south = bounds.getSouth();
			const north = bounds.getNorth();
			const west = bounds.getWest();
			const east = bounds.getEast();

			this._lastBounds = bounds;

			const size = this._map.getSize();

			const params = url.searchParams;

			params.set("crs", "http://www.opengis.net/def/crs/EPSG/0/3857");

			// The bbox is specified in CRS84 lat-lng, even when the
			// image is requested in EPSG:3857.
			// It would also be possible to specify the bbox in any other CRS,
			// by setting the `bbox-crs` parameter
			params.set("bbox", `${west},${south},${east},${north}`);
			// params.set("bbox-crs", "http://www.opengis.net/def/crs/EPSG/0/3857");

			params.set("width", size.x);
			params.set("height", size.y);
			params.set("transparent", !!this.options.transparent);

			this._image.onload = () => {
				this.setBounds(bounds);
			};

			this._image.src = url.toString();
		});
	},
});

L.imageOverlay.ogcapi = function (url, opts) {
	return new L.ImageOverlay.OGCAPI(url, opts);
};
